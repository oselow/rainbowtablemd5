import junit.framework.TestCase;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(JUnitParamsRunner.class)
public class ReduceTest extends TestCase {

    @Test
    @Parameters({
            "abcdefgh12345, abcdef1234",
            "toto1,toto1zz000 ",
            "ab12345cdefg, ab1234cdef"})
    public void testReduceFromBegening(String hash, String expected) throws Exception {
        assertEquals(expected, Reduce.reduceFromBegening(hash,4,6));
    }

    @Test
    @Parameters({
            "toto, otot",
            "tot10,01tot ",
            "Luc00, 00cuL"
    })
    public void testReverse(String word, String expected) throws Exception {
        assertEquals(expected, Reduce.reverse(word));
    }

    @Test
    @Parameters({
            "abcdefgh12345, 5432hgfedc",
            "toto1,1ototzz000 ",
            "ab12345cdefg, gfedc5432b"
    })
    public void testReduceFromEnd(String hash, String expected) throws Exception {
        assertEquals(expected, Reduce.reduceFromEnd(hash,4,6));
    }
}
