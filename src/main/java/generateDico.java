import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class generateDico {
    public static void main(String[] args) {
        JSONParser jsonParser = new JSONParser();

        write();
//
//        try (FileReader reader = new FileReader("D:\\RainbowTable\\src\\main\\java\\words.json")) {
//
//            Object words = jsonParser.parse(reader);
//
//            JSONArray wordsList = (JSONArray) words;
//
//            for (Object o : wordsList) {
//                System.out.println(o.toString());
//            }
//
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
    }

    public static void write() {
        JSONParser jsonParser = new JSONParser();
        try (FileReader reader = new FileReader("D:\\RainbowTable\\src\\main\\java\\words.json")) {
            Object words = jsonParser.parse(reader);
            JSONArray wordsList = (JSONArray) words;

            JSONArray finalList = new JSONArray();
            //TODO Generation des combinaisons mot + chiffres
            for (int i=0; i<10000 ; i++){
                for (Object o : wordsList) {
                    String myStr = o.toString();
                    int pos1 = new Random().nextInt(6);
                    int pos2 = new Random().nextInt(7);
                    int pos3 = new Random().nextInt(8);
                    int pos4 = new Random().nextInt(9);

                    int chiffre1 = new Random().nextInt(10);
                    int chiffre2 = new Random().nextInt(10);
                    int chiffre3 = new Random().nextInt(10);
                    int chiffre4 = new Random().nextInt(10);

                    int length = myStr.length();
                    myStr = myStr.substring(0, pos1).concat((Integer.toString(chiffre1))).concat(myStr.substring(pos1, length));
                    length = myStr.length();
                    myStr = myStr.substring(0, pos2).concat((Integer.toString(chiffre2))).concat(myStr.substring(pos2, length));
                    length = myStr.length();
                    myStr = myStr.substring(0, pos3).concat((Integer.toString(chiffre3))).concat(myStr.substring(pos3, length));
                    length = myStr.length();
                    myStr = myStr.substring(0, pos4).concat((Integer.toString(chiffre4))).concat(myStr.substring(pos4, length));

                    finalList.add(myStr);
                }
            }


            try (FileWriter file = new FileWriter("D:\\RainbowTable\\src\\main\\java\\wordss.json")) {

                file.write(finalList.toJSONString());
                file.flush();

            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
