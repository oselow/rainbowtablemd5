import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Random;

public class RainbowWithNumbers {

    private static int nbPasswords = 10000;
    private static int chainLength = 3000;
    private static final int maxNumbers = 4;
    private static final int maxLetters = 6;
    private static String selectedHash = "1b33fb7f610c82f9cb93a4b57ae7cb03";
    private static HashMap<String, String> matches = new HashMap<>();

    public static void main(String[] args) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        JSONParser jsonParser = new JSONParser();
        JSONArray wordsList = new JSONArray();

//        System.out.println(generatePass("word"));

        try (FileReader reader = new FileReader("C:\\INSSET\\rainbowtablemd5\\src\\main\\java\\words.json")) {
            Object words = jsonParser.parse(reader);

            wordsList = (JSONArray) words;

//            for (Object o : wordsList) {
//                System.out.println(o.toString());
//            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        int i = 0;
//        int nbPasswords = 2000;
//        int chainLength = 2000;
//        String selectedHash = "d900c6c4c668044ab478da453e021bdc";

//        HashMap<String, String> matches = new HashMap<>();

//        while (!found) {
        for (Object o : wordsList) {
            while (i < nbPasswords) {
                int j = 0;

                boolean check = false;

                String source = generatePass(o.toString());
                String result = source;

                while (j < chainLength) {
                    String hashed = hash(result);
                    result = Reduce.reduceFromBegening(hashed,maxNumbers,maxLetters);
                    j++;

                    if (hashed == selectedHash) {
                        check = true;
                    }
                }

                if (check) {
                    System.out.println("Hash trouvé " + source + " => " + result);
                }

                matches.put(result, source);
                i++;
            }
        }
//        }

//        System.out.println(find(selectedHash));
        System.out.println(find(selectedHash));


//        System.out.println(Reduce.reduceFromBegening("to5tdfsdfo"));
    }

    public static String hash(String str) throws NoSuchAlgorithmException {
        MessageDigest m = MessageDigest.getInstance("MD5");
        m.reset();
        m.update(str.getBytes());
        byte[] digest = m.digest();
        BigInteger bigInt = new BigInteger(1, digest);
        return bigInt.toString(16);
    }

    public static String generatePass(String str) {
        int pos1 = new Random().nextInt(6);
        int pos2 = new Random().nextInt(7);
        int pos3 = new Random().nextInt(8);
        int pos4 = new Random().nextInt(9);

        int chiffre1 = new Random().nextInt(10);
        int chiffre2 = new Random().nextInt(10);
        int chiffre3 = new Random().nextInt(10);
        int chiffre4 = new Random().nextInt(10);

        int length = str.length();
        str = str.substring(0, pos1).concat((Integer.toString(chiffre1))).concat(str.substring(pos1, length));
        length = str.length();
        str = str.substring(0, pos2).concat((Integer.toString(chiffre2))).concat(str.substring(pos2, length));
        length = str.length();
        str = str.substring(0, pos3).concat((Integer.toString(chiffre3))).concat(str.substring(pos3, length));
        length = str.length();
        str = str.substring(0, pos4).concat((Integer.toString(chiffre4))).concat(str.substring(pos4, length));

        return str;
    }


    public static String find(String input) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        int count = nbPasswords;

        System.out.println("Recharche dans le tableau ...");

        while (count > 0) {
            int x = 0;
            String reducedHash = Reduce.reduceFromBegening(input,maxNumbers,maxLetters);

            while (x < chainLength) {
                reducedHash = Reduce.reduceFromBegening(input,maxNumbers,maxLetters);
                input = hash(reducedHash);
                x += 1;
            }

            if (!matches.containsKey(reducedHash)) {
                count -= 1;
            } else {
                return findPlain(matches.get(reducedHash), input);
            }
        }


        throw new InternalError("Aucun résultat supplémentaire");
    }

    private static String findPlain(String input, String targetHash) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        System.out.println("Trouvé: " + input);
        System.out.println("Recherche '" + targetHash + "'...");

        String match = input;
        String current = hash(input);
        int count = 0;

        while (count < chainLength && !current.equals(targetHash)) {
            match = Reduce.reduceFromBegening(current,maxNumbers,maxLetters);
            current = hash(match);
        }

        if (count == chainLength) {
            throw new InternalError("Aucun resulatat");
        }
//        System.out.println(match);
        return match;
    }
}
