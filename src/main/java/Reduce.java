public class Reduce {

    public static String reduce(String str,int maxCountNumber, int maxCountLetter){
        int countNumbers = 0;
        int countLetters = 0;
        String res= "";

        for(int i=0; i < str.length(); i++) {
            if(Character.isDigit(str.charAt(i)) && countNumbers < maxCountNumber) {
                res = res.concat(Character.toString(str.charAt(i)));
                countNumbers ++;
            }
            else if(Character.isAlphabetic(str.charAt(i)) && countLetters < maxCountLetter){
                res = res.concat(Character.toString(str.charAt(i)));
                countLetters ++;
            }
        }

        return addMissingCharacters(countLetters, countNumbers, res, maxCountNumber, maxCountLetter);
    }

    public static String reverse(String str){
        String reversedStr = "";
        for(int i=str.length()-1; i >= 0; i--) {
            reversedStr = reversedStr.concat(Character.toString(str.charAt(i)));
        }
        return reversedStr;
    }

    public static String reduceFromBegening(String str,int maxCountNumber, int maxCountLetter){
        return reduce(str,maxCountNumber,maxCountLetter);
//        return str.substring(0,maxCountNumber+maxCountLetter);
    }

    public static String reduceFromEnd(String str,int maxCountNumber, int maxCountLetter){
        return reduce(reverse(str),maxCountNumber,maxCountLetter);
    }

    private static String addMissingCharacters(int countLetters, int countNumbers, String preRes, int maxCountNumber, int maxCountLetter){
        String res = preRes;
        for (int i=0; i<(maxCountLetter-countLetters); i++){
            res = res.concat("z");
        }
        for (int i=0; i<(maxCountNumber-countNumbers); i++){
            res = res.concat(Integer.toString(0));
        }

        return res;
    }

}
