import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Random;

public class RainbowWithoutNumber {
    private static int nbPasswords = 2000;
    private static int chainLength = 3000;
    private static String selectedHash = "7ddf32e17a6ac5ce04a8ecbf782ca509";
    private static final int maxNumbers = 0;
    private static final int maxLetters = 6;
    private static HashMap<String, String> matches = new HashMap<>();

    public static void main(String[] args) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        JSONParser jsonParser = new JSONParser();
        JSONArray wordsList = new JSONArray();

        try (FileReader reader = new FileReader("C:\\INSSET\\rainbowtablemd5\\src\\main\\java\\words.json")) {
            Object words = jsonParser.parse(reader);

            wordsList = (JSONArray) words;

//            for (Object o : wordsList) {
//                System.out.println(o.toString());
//            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        int i = 0;
//        int passwords = 10000;
//        int chainLength = 2000;
//        String selectedHash = "94a55a831108cd1eec9fb6016886b503";

//        HashMap<String, String> matches = new HashMap<>(passwords);

        while (i < nbPasswords) {
            int j = 0;

            boolean check = false;

            String source = wordsList.get(new Random().nextInt(wordsList.size())).toString();
            String result = source;

            while (j < chainLength) {
                String hashed = hash(result);
                result = Reduce.reduceFromBegening(hashed,maxNumbers,maxLetters);
                j++;

                if (hashed.equals(selectedHash)) {
                    check = true;
                }
            }

            if (check) {
                System.out.println("Hash trouvé " + source + " => " + result);
            }

            matches.put(result, source);
            i++;
        }

        System.out.println(find(selectedHash));

//        System.out.println(Reduce.reduceFromBegening("to5tdfsdfo"));
    }

    public static String hash(String str) throws NoSuchAlgorithmException {
        MessageDigest m = MessageDigest.getInstance("MD5");
        m.reset();
        m.update(str.getBytes());
        byte[] digest = m.digest();
        BigInteger bigInt = new BigInteger(1,digest);
        return bigInt.toString(16);
    }


    public static String find(String input) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        int count = nbPasswords - 1;

        System.out.println("Recharche dans le tableau ...");

        while (count >= 0) {
            int x = 0;
            String reducedHash = Reduce.reduceFromBegening(input,maxNumbers,maxLetters);

            while (x < chainLength) {
                reducedHash = Reduce.reduceFromBegening(input,maxNumbers,maxLetters);
                input = hash(reducedHash);
                x += 1;
            }

            if (!matches.containsKey(reducedHash)) {
                count -= 1;
            } else {
                return findPlain(matches.get(reducedHash), input);
            }
        }


        throw new InternalError("Aucun résultat supplémentaire");
    }

    private static String findPlain(String input, String targetHash) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        System.out.println("Trouvé: " + input);
        System.out.println("Recherche '" + targetHash + "'...");

        String match = input;
        String current = hash(input);
        int count = 0;

        while (count < chainLength && !current.equals(targetHash)) {
            match = Reduce.reduceFromBegening(current,maxNumbers,maxLetters);
            current = hash(match);
        }

        if (count == chainLength) {
            throw new InternalError("Aucun resulatat");
        }

        return match;
    }
}
